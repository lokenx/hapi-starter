'use strict';

const Code = require('code');
const Lab = require('lab');
const Server = require('../../server.js');

const internals = {};

const lab = exports.lab = Lab.script();
const expect = Code.expect;

lab.experiment('Example Plugin Tests', () => {

  lab.test('GET /test (endpoint test)', (done) => {

    const options = {
      method: 'GET',
      url: '/test'
    };

    Server.inject(options, (response) => {

      expect(response.statusCode).to.equal(200);
      Code.expect(response.result).to.have.length(11);
      Server.stop(done);
    });
  });
});
