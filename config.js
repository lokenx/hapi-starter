'use strict';

const config = module.exports = {};


config.monitor = {
  reporters: [
    {
      reporter: require('good-console'),
      events: { log: '*' },
      config: {
        format: 'YYYY/MM/DD HH:mm:ss.SS'
      }
    },
    {
      reporter: require('good-file'),
      events: { log: ['error', 'warn'] },
      config: './server.log'
    }
  ]
};
