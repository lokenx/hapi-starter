# Hapi Starter

Basic [Hapi JS](http://hapijs.com/) starter application. It's highly personal and may not be of use to you. It includes a basic [Glue](https://github.com/hapijs/glue) startup, [Good](https://github.com/hapijs/good) logging,
[Lab](https://github.com/hapijs/lab) tests, and a sample plugin.

Install the packages with `npm i` and start the server using `npm start`.

# License
This application is licensed under The MIT License.
