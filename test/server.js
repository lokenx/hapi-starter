'use strict';

const Code = require('code');
const Lab = require('lab');

const internals = {};

const lab = exports.lab = Lab.script();
const expect = Code.expect;

lab.experiment('Server Tests', () => {

  lab.test('Server startup', (done) => {

    const server = require('../server.js');

    expect(server.info.port).to.equal(8000);
    server.stop(done);
  });
});
