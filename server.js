'use strict';

const Glue = require('glue');
const Hoek = require('hoek');
const Config = require('./config');

const manifest = {
  connections: [
    {
      port: 8000
    }
  ],
  registrations: [
    {
      plugin: {
        register: 'good',
        options: Config.monitor
      }
    },
    {
      plugin: {
        register: './lib/plugins/example'
      }
    }
  ]
};

const options = {
  relativeTo: __dirname
};

Glue.compose(manifest, options, (err, server) => {

  Hoek.assert(!err, err);

  server.start(() => {

    server.log('info', `Hapi times at ${server.info.uri}`);
  });

  module.exports = server;
});
